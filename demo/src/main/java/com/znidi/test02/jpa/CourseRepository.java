package com.znidi.test02.jpa;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends CrudRepository<Course, Long>{
	
	
	List<Course> findByStudents_Id(long id);
	List<Course> findAll();
	List<Course> findByName(String name);
	List<Course> findByTeacherName(String name);
	
	Course findById(long id);
	
}
