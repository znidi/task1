package com.znidi.test02.jpa;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Long>{
	
	List<Student> findBySurname(String surname);
	
	List<Student> findByName(String name);

	Student findByUsername(String username);
	
	Student findByUsernameAndHash(String username, String hash);
	
	Student findById(long id);
}
