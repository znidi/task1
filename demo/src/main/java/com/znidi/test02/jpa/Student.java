package com.znidi.test02.jpa;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;

@Entity
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String username;
	private String hash;
	private String name;
	private String surname;
	
	@ManyToMany
	@JoinTable(
			  name = "course_students", 
			  joinColumns = @JoinColumn(name = "students_id"), 
			  inverseJoinColumns = @JoinColumn(name = "course_id"))
	private Set<Course> courses;
	
	protected Student() {}
	
	public Student(String username, String hash, String name, String surname) {
		this.username = username;
		this.hash = hash;
		this.name = name;
		this.surname = surname;
	}
	
	public void removeCourse(Course c) {
		this.courses.remove(c);
		c.getStudents().remove(this);
	}
	
	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getHash() {
		return hash;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}
	
	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", username=" + username + ", hash=" + hash + "]";
	}

}
