package com.znidi.test02.rest;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 * Exception to be thrown when student can't login
 * @author znidi
 *
 */
public class FalseUsernameOrHashException extends WebApplicationException {
	
	private static final long serialVersionUID = -8950500953239176238L;
	
	private String username;
	public FalseUsernameOrHashException(String username) {
		this.username = username;
	}
	
	/**
	 * Response to be returned when this error is intercepted by runtime
	 */
	@Override
	public Response getResponse() {
		return Response.status(403).entity("User " + username + " does not match the hash!").build();
	}
	
}
