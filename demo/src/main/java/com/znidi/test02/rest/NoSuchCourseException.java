package com.znidi.test02.rest;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 * Exception to be thrown when course does not exist
 * @author znidi
 *
 */
public class NoSuchCourseException extends WebApplicationException{

	private static final long serialVersionUID = 5678118252169385607L;

	private long courseId;
	
	public NoSuchCourseException(long courseId) {
		this.courseId = courseId;
	}
	
	/**
	 * Response to be returned when this error is intercepted by runtime
	 */
	@Override
	public Response getResponse() {
		return Response.status(404).entity("Course with id: " + courseId + " does not exist!").build();
	}
}
