package com.znidi.test02.rest;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.znidi.test02.jpa.CourseRepository;
import com.znidi.test02.jpa.Student;
import com.znidi.test02.jpa.StudentRepository;

/**
 * student resource
 * TODO: to be extended with other student functions
 * @author znidi
 *
 */
@Path("student")
public class StudentRes {
	@Autowired
	private StudentRepository studentRepository;
	@Autowired
	private CourseRepository courseRepository;
	@Context
	private UriInfo context;

	public StudentRes() {}
	
	/**
	 * classes sub-resource
	 * @param username
	 * @param hash
	 * @return
	 */
	@Path("classes")
	public Object classSub(@HeaderParam("username") String username, @HeaderParam("hash") String hash) {
		try {
		    DecodedJWT jwt = JWT.decode(hash);
		    String pass = jwt.getClaim("password").asString();
		    Student tmp = studentRepository.findByUsernameAndHash(username, pass);
			if(tmp==null) 
				throw new FalseUsernameOrHashException(username);
			return new ClassSub(tmp, studentRepository, courseRepository);
		} catch (JWTDecodeException e){
		    e.printStackTrace();
		}
		throw new FalseUsernameOrHashException(username);
	}

}
