package com.znidi.test02.rest;

import java.util.List;
import java.util.Set;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.stereotype.Component;

import com.znidi.test02.jpa.Course;
import com.znidi.test02.jpa.CourseRepository;
import com.znidi.test02.jpa.Student;
import com.znidi.test02.jpa.StudentRepository;

/**
 * class sub-resource intended for manipulation of classes in relation to
 * student
 * 
 * @author znidi
 *
 */
@Component
public class ClassSub {

	private StudentRepository studentRepository;
	private CourseRepository courseRepository;
	@Context
	private UriInfo context;

	Student student;

	public ClassSub() {
	}

	/**
	 * repositories need to be specified since we created the object manually
	 * therefore @Autowired will not work
	 * 
	 * @param student
	 * @param studentRepository
	 * @param courseRepository
	 */
	public ClassSub(Student student, StudentRepository studentRepository, CourseRepository courseRepository) {
		this.student = student;
		this.studentRepository = studentRepository;
		this.courseRepository = courseRepository;
	}

	/**
	 * returns all existing classes
	 * 
	 * @return
	 */
	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		List<Course> res = courseRepository.findAll();
		if (res == null || res.size() == 0)
			return Response.status(404).entity("no courses found").build();
		return Response.ok().entity(res.toString()).build();
	}

	/**
	 * returns classes enrolled by this student
	 * 
	 * @return
	 */
	@GET
	@Path("enrolled")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEnroled() {
		Set<Course> res = student.getCourses();
		if (res == null || res.size() == 0)
			return Response.status(404).entity("no courses found").build();
		return Response.ok().entity(res.toString()).build();
	}

	@GET
	@Path("available")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAvailable() {
		List<Course> rem = courseRepository.findAll();
		rem.removeAll(student.getCourses());
		if(rem==null||rem.size()==0)
			return Response.status(404).entity("no courses found").build();
		return Response.ok().entity(rem.toString()).build();// TODO: fix
	}

	/**
	 * enroll student to the class
	 * 
	 * @param classID
	 * @param body
	 * @return
	 */
	@POST
	@Path("enroll/{classID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response enroll(@PathParam("classID") long classID, String body) {
		Course courseToAdd = courseRepository.findById(classID);
		if (courseToAdd == null)
			throw new NoSuchCourseException(classID);
		student.getCourses().add(courseToAdd);
		studentRepository.save(student);
		return Response.status(201).entity(courseToAdd.toString()).build();
	}

	/**
	 * remove enrollment from this student
	 * 
	 * @param classID
	 * @param body
	 * @return
	 */
	@DELETE
	@Path("enroll/{classID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response cancel(@PathParam("classID") long classID, String body) {
		Course courseToRemove = courseRepository.findById(classID);
		if (courseToRemove == null)
			throw new NoSuchCourseException(classID);
		student.removeCourse(courseToRemove);
		studentRepository.save(student);
		courseRepository.save(courseToRemove);
		return Response.status(200).build();
	}

	@GET
	@Path("search/name/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByName(@PathParam("name") String name) {
		List<Course> res = courseRepository.findByName(name);
		if (res == null || res.size() == 0)
			return Response.status(404).entity("no courses found").build();
		return Response.ok().entity(res.toString()).build();
	}

	@GET
	@Path("search/teacher/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByTeacherName(@PathParam("name") String name) {
		List<Course> res = courseRepository.findByTeacherName(name);
		if (res == null || res.size() == 0)
			return Response.status(404).entity("no courses found").build();
		return Response.ok().entity(res.toString()).build();
	}

}
